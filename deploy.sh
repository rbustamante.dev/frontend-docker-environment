#!/bin/bash
if [[ $1 == "dev" && $2 == "up" || $1 == "pro" && $2 == "up" ]]; then
    echo
    echo "> STARTED <"
    echo
    docker-compose -f ./deploy/${1}.docker-compose.yaml build --no-cache && docker-compose -f ./deploy/${1}.docker-compose.yaml $2
elif [[ $1 == "dev" && $2 == "down" || $1 == "pro" && $2 == "down" ]]; then
    echo
    echo "> STOPPED <"
    echo
    docker-compose -f ./deploy/${1}.docker-compose.yaml $2 && yes | docker image prune -a
else
    echo
    echo "> Para iniciar ambiente de desarrollo: ./deploy.sh dev up"
    echo "> Para detener ambiente de desarrollo: ./deploy.sh dev down"
    echo
    echo "> Para iniciar ambiente de produccion: ./deploy.sh pro up"
    echo "> Para detener ambiente de produccion: ./deploy.sh pro down"    
fi
