# STEP 1 - Build node image
FROM node:18.14.0-alpine

# STEP 1 - Set directory
WORKDIR /app

# STEP 1 - Copy project files
COPY ./moorea/package.json /app

# STEP 1 - Install dependencies
RUN cd /app && npm set progress=false && npm install

# STEP 1 - Copy project files after install dependencies
COPY ./moorea /app

# STEP 2 - Run
CMD ["npm", "run", "start:dev"]