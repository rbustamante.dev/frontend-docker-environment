/** @type {import('tailwindcss').Config} */

module.exports = {
    presets: [
        require('./main.style.cjs')
    ],
    theme: {
        extend: {
            colors: {
                'primary': '#1fb645',
                'secondary': '#4388CC',
            }
        },
    },
}
