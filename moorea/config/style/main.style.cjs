/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    "./index.html",
    "./src/components/interfaces/**/*.{ts,tsx}",
    "./src/screens/**/*.{ts,tsx}"
  ],
  theme: {
    extend: {
    },
  },
  plugins: [],
}
