import { useState } from "react";
import leafnoise_logo from "../assets/leafnoise.svg";
import { useShallow, useStore } from "../../states/stores/zustandStore";

export const PruebaInterface = (): JSX.Element => {

  // ZUSTAND
  const user = useStore((state) => { return state.user; }, useShallow);
  const product = useStore((state) => { return state.product; }, useShallow);

  console.log("USER", user);
  console.log("PRODUCT", product);

  const [count, setCount] = useState(0);

  const fetchUser = () => {
    user.getUsers();
  };

  const tema_actual: string = import.meta.env.ENV_STYLE as string; // correcto
  const env_actual = import.meta.env.ENV_MODE; // incorrecto, unsafe any

  return (
    <>
      <div className="flex flex-col items-center justify-center bg-secondary w-full h-screen">
        <div>
          <img src={leafnoise_logo} className="w-80 mb-12" alt="logo" />
        </div>
        <h1 className='text-slate-100 mb-12 text-2xl'>Vite + React + Tailwind = Moorea Workbench</h1>
        <p className='text-slate-100 mb-2 text-lg'>DEV: Esbuild (pre empaquetado) + ESM (sirve codigo fuente nativo y lo empaqueta el navegador)</p>
        <p className='text-slate-100 mb-12 text-lg'>PRO: Rollup (empaquetado) + Postcss (transpila el codigo javascript (tailwind) a css)</p>
        <div className="flex flex-col items-center justify-center">
          <button onClick={() => { return setCount((count) => { return count + 1; }); }} className="text-slate-100 p-2 border-2 border-slate-200 rounded mb-6 bg-primary">
            Cuenta {count}
          </button>
          <p className='text-slate-100 text-lg'>Reemplazo de módulo caliente (HMR)</p>
          <p className='text-slate-100 text-lg'>Configuracion de lint en base al entorno</p>
          <p className='mb-6 text-slate-100 text-lg'>Configuración de estilos dinámicos</p>
          <p className='text-slate-100 text-lg font-medium'>Entorno: {env_actual}</p>
          <p className='mb-6 text-slate-100 text-lg font-medium'>Estilo: {tema_actual}</p>
          <button onClick={() => { fetchUser(); }} className="text-slate-100 p-2 border-2 border-slate-200 rounded mb-6 bg-primary">
            Pegale a la API
          </button>
          <p className='text-slate-100 text-lg font-medium'>Store: {user.status === "success" ? user.data.name : user.status}</p>
        </div>
      </div>
    </>
  );
};
