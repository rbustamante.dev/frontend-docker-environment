import ReactDOM from "react-dom/client";
import { RouterProvider } from "react-router-dom";
import { router } from "./routes/router";
import { MainInterceptor } from "./services/interceptors/mainInterceptor";
import axios from "axios";
import "./main.css";

MainInterceptor(axios);
ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <RouterProvider router={router}></RouterProvider>
);
