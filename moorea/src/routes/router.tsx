import { createBrowserRouter } from "react-router-dom";
import { MainScreen } from "../screens/screensModule";

export const router = createBrowserRouter([
  { path: "/", element: <MainScreen></MainScreen> },
]);
