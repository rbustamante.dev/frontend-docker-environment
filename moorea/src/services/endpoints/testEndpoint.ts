import axios, { AxiosResponse } from "axios";

export const getDataAPI = async (): Promise<AxiosResponse> => {
  return await axios.get("https://rickandmortyapi.com/api/character/2");
};