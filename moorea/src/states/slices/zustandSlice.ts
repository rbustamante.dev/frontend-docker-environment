import { getDataAPI } from "../../services/endpoints/testEndpoint";
import { StateCreator } from "zustand";

export type UserSlice = {
  user: {
    data: { name: string },
    status: string,
    getUsers: () => Promise<void>,
  }
}

export const userSlice: StateCreator<UserSlice> = (set) => {
  return {
    user: {
      data: { name: "" },
      status: "idle",
      getUsers: async () => {
        set((state) => { return { user: { ...state.user, status: "loading" } }; });
        const response = await getDataAPI().then((response) => { return response.data; });
        set((state) => { return { user: { ...state.user, data: response } }; });
        set((state) => { return { user: { ...state.user, status: "success" } }; });
      },
    }
  };
};

export type ProductSlice = {
  product: {
    data: object,
    status: string,
    getProducts: () => Promise<void>,
  }
}

export const productSlice: StateCreator<ProductSlice> = (set) => {
  return {
    product: {
      data: {},
      status: "idle",
      getProducts: async () => {
        set((state) => { return { product: { ...state.product, status: "loading" } }; });
        const response = await getDataAPI().then((response) => { return response.data; });
        set((state) => { return { product: { ...state.product, data: response } }; });
        set((state) => { return { product: { ...state.product, status: "success" } }; });
      },
    }
  };
};