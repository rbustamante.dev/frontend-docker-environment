import { create } from "zustand";
import { shallow } from "zustand/shallow";
import { ProductSlice, productSlice, userSlice, UserSlice } from "../slices/zustandSlice";

export const useStore = create<UserSlice & ProductSlice>((...state) => {
  return {
    ...userSlice(...state),
    ...productSlice(...state),
  };
});

export const useShallow = shallow;