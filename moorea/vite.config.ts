import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

export default defineConfig({
  root: ".",
  server: {
    host: true,
    watch: {
      usePolling: true,
    },
    port: 5000,
    strictPort: true,
  },
  build: {
    emptyOutDir: true,
    manifest: false,
    outDir: './build',
    assetsDir: "static",
    rollupOptions: {
      input: {
        index: 'index.html',
      },
      output: {
        sourcemap: false,
        inlineDynamicImports: true
      }
    },
    reportCompressedSize: false,
    chunkSizeWarningLimit: 900,
  },
  css: {
    postcss: {
      plugins: [{
        ...(process.env.ENV_MODE === "PRO" ? require("cssnano")({}) : {}),
        ...(process.env.ENV_MODE === "PRO" ? require('autoprefixer')({}) : {}),
        ...(process.env.ENV_STYLE === 'anses' ? require("tailwindcss")({ config: "./config/style/anses.style.cjs" }) : {}),
        ...(process.env.ENV_STYLE === 'afip' ? require("tailwindcss")({ config: "./config/style/afip.style.cjs" }) : {})
      }]
    }
  },
  publicDir: false,
  clearScreen: false,
  envPrefix: "ENV_",
  plugins: [react()]
})
